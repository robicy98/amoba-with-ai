package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import static java.lang.Math.min;
import static java.lang.StrictMath.max;


public class AmobaController implements Initializable {

    @FXML
    Pane Pane;

    private Integer xmin = 15;
    private Integer xmax = -1;
    private Integer ymin = 15;
    private Integer ymax = -1;
    private ArrayList<Button> buttons = new ArrayList<Button>() ;
    private double size;
    private int k;
    private int num;
    private int matrix[][] = {
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0}
    };

    private void setScreen(){

        String fileName = "src/sample/output.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                num = Integer.parseInt(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        size = 420/num;
        k = min(5,num);

        for(int i = 0;i < num; i++){
            for (int j = 0;j < num; j++) {
                Button b = new Button("");
                b.setLayoutX(j * size + 65);
                b.setLayoutY(i * size + 165);
                b.setMinSize(size,size);
                buttons.add(b);
                b.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {
                        if (b.getText() == ""){
                            click(b);
                        }
                    }
                });
                Pane.getChildren().add(b);
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setScreen();
    }

    private void click(Button b){
        Integer i = (int)((b.getLayoutY()-165)/size);
        Integer j = (int)((b.getLayoutX()-65)/size);
        Pair<Integer,Integer> tmp = new Pair<Integer, Integer>(-1,-1);
        playerX(i,j);


        if (checkTheWin() == -1){
            System.out.println("X Win");
            done();
        }
        if (checkTheWin() == 1){
            System.out.println("O win");
            done();
        }
        if (!moveLeft()){
            System.out.println("Draw");
            done();
        }


        tmp = bestMove();
        if ((tmp.getKey() != -1) && (tmp.getValue() != -1)) {
            i = tmp.getKey();
            j = tmp.getValue();
            playerO(i, j);
        }

        if (checkTheWin() == -1){
            System.out.println("X Win");
            done();
        }
        if (checkTheWin() == 1){
            System.out.println("O win");
            done();
        }
        if (!moveLeft()){
            System.out.println("Draw");
            done();
        }

    }

    private boolean moveLeft(){
        for (int i = ymin;i <= ymax; i++)
            for (int j = xmin; j <= xmax; j++){
                if (matrix[i][j] == 0)
                    return true;
            }
        return false;
    }

    private void done(){
        Button b;
        for (int i = 0; i < num*num;i++){
            b = buttons.get(i);
            b.setDisable(true);
        }
    }

    private void playerX(Integer x, Integer y){
        matrix[x][y] = -1;
        xmin = min(x-1,xmin);
        xmin = max(xmin,0);

        xmax = max(x+1,xmax);
        xmax = min(xmax,num-1);

        ymin = min(y-1,ymin);
        ymin = max(ymin,0);

        ymax = max(y+1,ymax);
        ymax = min(ymax,num-1);

        Button b = buttons.get(x*num+y);
        b.setText("X");
    }

    private void playerO(Integer x, Integer y){
        matrix[x][y] = 1;

        xmin = min(x-1,xmin);
        xmin = max(xmin,0);

        xmax = max(x+1,xmax);
        xmax = min(xmax,num-1);

        ymin = min(y-1,ymin);
        ymin = max(ymin,0);

        ymax = max(y+1,ymax);
        ymax = min(ymax,num-1);

        Button b = buttons.get(x*num+y);
        b.setText("O");
    }

    private boolean rowWin(int a){
        for (int i = 0; i < num; i++){
            int number = 0;
            for (int j = 0;j < num;j++){
                if (matrix[i][j] == a){
                    number++;
                }
                else{
                    number = 0;
                }
                if (number == k){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean colWin(int a){
        for (int i = 0; i < num; i++){
            int number = 0;
            for (int j = 0;j < num;j++){
                if (matrix[j][i] == a){
                    number++;
                }
                else{
                    number = 0;
                }
                if (number == k){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean diagonalWin(int a){
        int tmp = num-k+1;
        int scale = - (tmp/2);
        for (int i = 0;i < (2*tmp-1) ;i++){
            int t = 0;
            int f = 0;
            for (int j = 0; j < num;j++){
                for (int l = 0; l < num; l++){
                    if (j == l + scale){
                        if (matrix[j][l] == a){
                            t++;
                        }else{
                            t = 0;
                        }
                    }
                    if (j+l == num-1+scale){
                        if (matrix[j][l] == a){
                            f++;
                        }else{
                            f = 0;
                        }
                    }
                    if (f == k || t == k)
                        return true;
                }
            }
            scale++;
        }
        return false;
    }

    private Integer checkTheWin() {
        if (rowWin(1) || colWin(1) || diagonalWin(1))
            return 1;
        if (rowWin(-1) || colWin(-1) || diagonalWin(-1))
            return -1;
        return 0;
    }

    private Integer evaluateFunctionCall(){
        Integer a = checkTheWin();
        if (a == 0){
            return (evaluateFunction(1)+evaluateFunction(-1));
        }else if (a == 1){
            return 999999;
        }else {
            return -999999;
        }
    }

    private Integer getRowOneNumber(int a,int i){
        int num = 0;
        for (int j = xmin;j <= xmax;j++){
            if (j != 0 && j != 11 && matrix[i][j] == a && matrix[i][j-1] != a && matrix[i][j+1] != a){
                num++;
            }
        }
        return num;
    }

    private Integer getRowTwoNumber(int a, int i){
        int num = 0;
        for (int j = xmin;j <= xmax-1;j++){
            if (j != 0 && j != 11 && j != 10 &&
                matrix[i][j] == a &&
                matrix[i][j-1] != a &&
                matrix[i][j+1] == a &&
                matrix[i][j+2] != a){
                num++;
            }
        }
        return num;
    }

    private Integer getRowThreeNumber(int a, int i){
        int num = 0;
        for (int j = xmin;j <= xmax-2;j++){
            if (j != 0 && j != 11 &&
                matrix[i][j] == a &&
                matrix[i][j-1] != a &&
                matrix[i][j+1] == a &&
                matrix[i][j+2] == a &&
                matrix[i][j+3] != a){
                num++;
            }
        }
        return num;
    }

    private Integer getRowFourNumber(int a, int i){
        int num = 0;
        for (int j = xmin;j <= xmax-3;j++){
            if (j != 0 && j != 11 &&
                matrix[i][j] == a &&
                matrix[i][j-1] != a &&
                matrix[i][j+1] == a &&
                matrix[i][j+2] == a &&
                matrix[i][j+3] == a &&
                matrix[i][j+4] != a){
                num++;
            }
        }
        return num;
    }

    private Integer getColOneNumber(int a, int j){
        int num = 0;
        for (int i = ymin;i<=ymax;i++){
            if (i != 0 && i != 11 &&
                matrix[i][j] == a &&
                matrix[i-1][j] != a &&
                matrix[i+1][j] != a)
                num++;
        }
        return num;
    }

    private Integer getColTwoNumber(int a, int j){
        int num = 0;
        for (int i = ymin;i<=ymax-1;i++){
            if (i != 0 &&
                i != 11 &&
                i != 10 &&
                matrix[i][j] == a &&
                matrix[i-1][j] != a &&
                matrix[i+1][j] == a &&
                matrix[i+2][j] != a)
                num++;
        }
        return num;
    }

    private Integer getColThreeNumber(int a, int j){
        int num = 0;
        for (int i = ymin;i<=ymax-2;i++){
            if (i != 0 && i != 11 &&
                matrix[i][j] == a &&
                matrix[i-1][j] != a &&
                matrix[i+1][j] == a &&
                matrix[i+2][j] == a &&
                matrix[i+3][j] != a)
                num++;
        }
        return num;
    }

    private Integer getColFourNumber(int a, int j){
        int num = 0;
        for (int i = ymin;i<=ymax-3;i++){
            if (i != 0 && i != 11 &&
                matrix[i][j] == a &&
                matrix[i-1][j] != a &&
                matrix[i+1][j] == a &&
                matrix[i+2][j] == a &&
                matrix[i+3][j] == a &&
                matrix[i+4][j] != a)
                num++;
        }
        return num;
    }

    private Integer getDiagLength(int a,int i,int j){
        if (i > 0 && j > 0 && matrix[i-1][j-1] != a){
            if (matrix[i][j] == a){
                if (i < 11 && j < 11 && matrix[i+1][j+1] == a){
                    if (i < 10 && j < 10 && matrix[i+2][j+2] == a){
                        if (i < 9 && j < 9&& matrix[i+3][j+3] == a){
                            return 4;
                        }else{
                            return 3;
                        }
                    }else
                        return 2;
                }else{
                    return 1;
                }
            }else{
                return 0;
            }
        }else
        return 0;
    }

    private Integer getSecoundDiagonalLength(int a,int i,int j){
        if (i > 0 && j < 11 && matrix[i-1][j+1] != a) {
            if (matrix[i][j] == a) {
                if (i < 11 && j > 0 && matrix[i+1][j-1] == a){
                    if (i < 10 && j > 1 && matrix[i+2][j-2] == a){
                        if (i < 9 && j > 2 && matrix[i+3][j-3] == a){
                            return 4;
                        }else{
                            return 3;
                        }
                    }else
                        return 2;
                }else{
                    return 1;
                }
            }else{
                return 0;
            }
        }else
            return 0;
    }

    private Integer evaluateFunction(int a){
        int a1,a2,a3,a4,tmp;
        a4 = 0;
        tmp=0;
        for (int i = ymin;i <= ymax;i++){
            a1 = getRowOneNumber(a,i);
            a2 = getRowTwoNumber(a,i);
            a3 = getRowThreeNumber(a,i);
            if (k > 4)
                a4 = getRowFourNumber(a,i);
            tmp += a1+a2*10+a3*100+a4*1000;
        }
        a4 = 0;
        for (int i = xmin;i<=xmax;i++){
            a1 = getColOneNumber(a,i);
            a2 = getColTwoNumber(a,i);
            a3 = getColThreeNumber(a,i);
            if (k > 4){
                a4 = getColFourNumber(a,i);
            }
            tmp += a1+10*a2+100*a3+1000*a4;
        }
        for (int i = ymin;i <= ymax;i++){
            for (int j = xmin;j<=xmax;j++) {
                a4 = getDiagLength(a,i,j);
                    if (a4 == 1)
                        tmp++;
                    if (a4 == 2)
                        tmp+=10;
                    if (a4 == 3)
                        tmp+=100;
                    if (a4 == 4)
                        tmp+=1000;
                }
            }
        for (int i = ymin;i <= ymax;i++){
            for (int j = xmin;j<=xmax;j++) {
                a4 = getSecoundDiagonalLength(a,i,j);
                if (a4 == 1)
                    tmp++;
                if (a4 == 2)
                    tmp+=10;
                if (a4 == 3)
                    tmp+=100;
                if (a4 == 4)
                    tmp+=1000;
            }
        }
            return a*tmp;
        }

    private int miniMax(boolean isMax,int depth, int alpha, int beta){
        int score = evaluateFunctionCall();

        if (!moveLeft())
            return score;

        if (depth <= 0)
            return score;

        if (isMax){

            for (int i = ymin;i <=ymax; i++) {

                for (int j = xmin; j <= xmax; j++) {

                    if (matrix[i][j] == 0){

                        matrix[i][j] = 1;

                        if (beta >= alpha) {
                            alpha = Math.max(alpha, miniMax(false, --depth, alpha, beta));
                        }
                        matrix[i][j] = 0;
                    }
                }
            }
            return alpha;
        }else{

            for (int i = ymin;i <=ymax; i++) {

                for (int j = xmin; j <= xmax; j++) {

                    if (matrix[i][j] == 0){

                        matrix[i][j] = -1;
                        if (beta >= alpha) {
                            beta = Math.min(beta, miniMax(true, --depth, alpha, beta));
                        }
                        matrix[i][j] = 0;
                    }
                }
            }
            return beta;
        }
    }

    private Pair<Integer,Integer> bestMove(){
        int bestVal = -999999;
        Pair<Integer,Integer> tmp = new Pair<Integer, Integer>(-1,-1);


        for (int i = ymin;i <=ymax; i++) {
            for (int j = xmin; j <=xmax; j++) {
                if (matrix[i][j] == 0){

                    matrix[i][j] = 1;
                    int depth = 4;
                    int moveVal = miniMax(false, depth,-999999,999999);
                    matrix[i][j] = 0;

                    if (moveVal > bestVal){
                        tmp = new Pair<Integer, Integer>(i,j);
                        bestVal = moveVal;
                    }

                }
            }
        }
        return tmp;
    }
}
