package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


//O = 1
//X = -1

public class TTTcontroller implements Initializable {

    @FXML
    Pane pane;

    private ArrayList<Button> buttons = new ArrayList<Button>();
    private int n = 3;
    private int matrix[][] = {{0,0,0},{0,0,0},{0,0,0}};

    private void setScreen(){
        for (int i = 0; i < n; i++){
            for (int j = 0;j < n;j++) {
                Button b = new Button();
                b.setLayoutX(j * 420/n + 65);
                b.setLayoutY(i * 420/n + 165);
                b.setMinSize(420/n,420/n);
                buttons.add(b);
                b.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {
                        if (b.getText() == ""){
                            click(b);
                        }
                    }
                });
                pane.getChildren().add(b);
            }
        }
    }

    private void click(Button b){
        int i = (int) (b.getLayoutY()-165)/140;
        int j = (int) (b.getLayoutX()-65)/140;
        matrix[i][j] = -1;
        b.setText("X");

        int coord= bestMove();
        if (coord != -1) {
            i = coord / 10;
            j = coord % 10;
            matrix[i][j] = 1;
            b = buttons.get(i * n + j);
            b.setText("O");
        }


        if (winTest() == -1){
            System.out.println("X win");
            done();
        }
        if (winTest() == 1) {
            System.out.println("O win");
            done();
        }
        if (!moveLeft()){
            System.out.println("Draw");
            done();
        }
    }

    private void done(){
        Button b;
        for (int i = 0; i < 9;i++){
            b = buttons.get(i);
            b.setDisable(true);
        }
    }

    private int winTest(){
        for (int row = 0; row<3; row++)
        {
            if (matrix[row][0]==matrix[row][1] &&
                    matrix[row][1]==matrix[row][2])
            {
                if (matrix[row][0]==1)
                    return +1;
                else if (matrix[row][0]==-1)
                    return -1;
            }
        }
        for (int col = 0; col<3; col++) {
            if (matrix[0][col] == matrix[1][col] &&
                    matrix[1][col] == matrix[2][col]) {
                if (matrix[0][col] == 1)
                    return +1;

                else if (matrix[0][col] == -1)
                    return -1;
            }
        }

        if (matrix[0][0]==matrix[1][1] && matrix[1][1]==matrix[2][2])
        {
            if (matrix[0][0]==1)
                return +1;
            else if (matrix[0][0]==-1)
                return -1;
        }

        if (matrix[0][2]==matrix[1][1] && matrix[1][1]==matrix[2][0])
        {
            if (matrix[0][2]==1)
                return +1;
            else if (matrix[0][2]==-1)
                return -1;
        }

        return 0;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setScreen();
    }

    private boolean moveLeft(){
        for (int i = 0;i < n; i++)
            for (int j = 0; j < n; j++){
                if (matrix[i][j] == 0)
                    return true;
            }
        return false;
    }

    private int miniMax(boolean isMax){
        int score = winTest();

        if (score == 1)
            return 10;

        if (score == -1)
            return -10;

        if (!moveLeft())
            return score;

        if (isMax){

            int best = -1000;

            for (int i = 0;i < n; i++) {

                for (int j = 0; j < n; j++) {

                    if (matrix[i][j] == 0){

                        matrix[i][j] = 1;

                        best = Math.max(best,miniMax(false));

                        matrix[i][j] = 0;
                    }
                }
            }
            return best;
        }else{
            int best = 1000;

            for (int i = 0;i < n; i++) {

                for (int j = 0; j < n; j++) {

                    if (matrix[i][j] == 0){

                        matrix[i][j] = -1;

                        best = Math.min(best,miniMax(true));

                        matrix[i][j] = 0;
                    }
                }
            }
            return best;
        }
    }

    private int bestMove(){
        int bestVal = -1000;
        int tmp = -1;

        for (int i = 0;i < n; i++){
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0){

                    matrix[i][j] = 1;
                    int moveVal = miniMax(false);
                    matrix[i][j] = 0;

                    if (moveVal > bestVal){
                        tmp = 10*i+j;
                        bestVal = moveVal;
                    }

                }
            }
        }
        return tmp;
    }

}
