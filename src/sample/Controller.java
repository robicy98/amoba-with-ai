package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Controller {

    @FXML
    Button StartButton;

    @FXML
    Button ExitButton;

    @FXML
    Label DifficultyLabel;

    @FXML
    Slider DifficultySlider;

    public void drag(){
        DifficultyLabel.setText(String.valueOf((int)DifficultySlider.getValue()));
    }

    public void exit(ActionEvent event){
        Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        gameStage.close();
    }

    public void start(ActionEvent event) throws IOException {
        if ((int)DifficultySlider.getValue() == 3) {
            Parent game = FXMLLoader.load(getClass().getResource("3x3.fxml")); //betolt
            Scene gameScene = new Scene(game, 550, 750);
            Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            gameStage.setScene(gameScene);
            gameStage.show();
        }
        else{
            Path path = Paths.get("src/sample/output.txt");

            Integer size = (int)DifficultySlider.getValue();

            try (BufferedWriter writer = Files.newBufferedWriter(path))
            {
                writer.write(""+size);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent game = FXMLLoader.load(getClass().getResource("bigGame.fxml")); //betolt
            Scene gameScene = new Scene(game, 550, 750);
            Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            gameStage.setScene(gameScene);
            gameStage.show();
        }
    }
}
